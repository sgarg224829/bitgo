package org.example;

import org.example.controller.NotificationController;
import org.example.model.Notification;
import org.example.model.NotificationStatus;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println("Crypto Notification service!");

        Notification notification1 = new Notification(25.3, 4.3, 6000);
        Notification notification2 = new Notification(28.3, 9.3, 10000);
        Notification notification3 = new Notification(85.3, 1.3, 63000);
        Notification notification4 = new Notification(225.3, 0.43, 16000);
        Notification notification5 = new Notification(250.3, 14.3, 60000);

        NotificationController notificationController = new NotificationController();

        //Create Notification
        notificationController.createNotification(notification1);
        notificationController.createNotification(notification2);
        notificationController.createNotification(notification3);
        notificationController.createNotification(notification4);
        notificationController.createNotification(notification5);

        //Send Email
        List<String> emails = Arrays.asList("abc@gmail.com", "pqr@gmail.com");
        notificationController.sendEmail(emails, notification1);
        notificationController.sendEmail(emails, notification2);
        notificationController.sendEmail(emails, notification3);

        //Update Notification
        notification1.setNotificationStatus(NotificationStatus.SENT);
        notification2.setNotificationStatus(NotificationStatus.FAILED);
        notification3.setNotificationStatus(NotificationStatus.SENT);
        notificationController.updateNotification(notification1);
        notificationController.updateNotification(notification2);
        notificationController.updateNotification(notification3);


        //Delete Notification
        notificationController.deleteNotification(notification4.getNotificationId());

        //List Notifications
        List<Notification> sentNotifications = notificationController.getNotifications(NotificationStatus.SENT);
        System.out.println("Sent Notifications are : " + sentNotifications.size()); //2

        List<Notification> pendingNotifications = notificationController.getNotifications(NotificationStatus.PENDING);
        System.out.println("pending Notifications are : " + pendingNotifications.size()); //1

        List<Notification> failedNotifications = notificationController.getNotifications(NotificationStatus.FAILED);
        System.out.println("Failed Notifications are : " + failedNotifications.size()); //1

        List<Notification> deletedNotifications = notificationController.getNotifications(NotificationStatus.DELETED);
        System.out.println("Deleted Notifications are : " + deletedNotifications.size()); //1

    }
}