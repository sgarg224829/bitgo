package org.example.model;

public enum NotificationStatus {

    PENDING, SENT, FAILED, DELETED;
}
