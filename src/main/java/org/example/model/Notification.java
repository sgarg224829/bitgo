package org.example.model;

import java.util.UUID;

public class Notification {

    private final String notificationId;

    private Double bitcoinPrice;

    private Double dailyPercentageChange;

    private Integer tradingVolume;

    private NotificationStatus notificationStatus;

    public Notification(double bitcoinPrice, double dailyPercentageChange, int tradingVolume){
        this.notificationId = UUID.randomUUID().toString();
        this.bitcoinPrice = bitcoinPrice;
        this.dailyPercentageChange = dailyPercentageChange;
        this.tradingVolume = tradingVolume;
        this.notificationStatus = NotificationStatus.PENDING;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public Double getBitcoinPrice() {
        return bitcoinPrice;
    }

    public void setBitcoinPrice(double bitcoinPrice) {
        this.bitcoinPrice = bitcoinPrice;
    }

    public Double getDailyPercentageChange() {
        return dailyPercentageChange;
    }

    public void setDailyPercentageChange(double dailyPercentageChange) {
        this.dailyPercentageChange = dailyPercentageChange;
    }

    public Integer getTradingVolume() {
        return tradingVolume;
    }

    public void setTradingVolume(int tradingVolume) {
        this.tradingVolume = tradingVolume;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }
}
