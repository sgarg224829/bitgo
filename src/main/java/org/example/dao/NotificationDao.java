package org.example.dao;

import org.example.model.Notification;
import org.example.model.NotificationStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class NotificationDao {

    Logger logger = Logger.getLogger(NotificationDao.class.getName());

    List<Notification> notificationList;

    public NotificationDao(){
        this.notificationList = new ArrayList<>();
    }

    public void addNotification(Notification notification){
        notificationList.add(notification);
        logger.info("Notification with Id : " + notification.getNotificationId() + "is added to DB");
    }

    public Notification updateNotification(Notification notification){
        String notificationId = notification.getNotificationId();
        Optional<Notification> existingNotification =
                notificationList.stream().filter(nt -> nt.getNotificationId().equals(notificationId)).findAny();
        if(existingNotification.isEmpty()){
            logger.warning("NO existing notification found with notification ID : " + notificationId);
            return null;
        }else{
            Notification olderNotification = existingNotification.get();
            notificationList.remove(olderNotification);
            if(notification.getTradingVolume() != null){
                olderNotification.setTradingVolume(notification.getTradingVolume());
            }

            if(notification.getBitcoinPrice() != null){
                olderNotification.setBitcoinPrice(notification.getBitcoinPrice());
            }

            if(notification.getDailyPercentageChange() != null){
                olderNotification.setDailyPercentageChange(notification.getDailyPercentageChange());
            }

            notificationList.add(olderNotification);
            return olderNotification;
        }
    }

    public  List<Notification> getNotifications(NotificationStatus status){
        return notificationList.stream().filter(notification ->  notification.getNotificationStatus() == status)
                .collect(Collectors.toList());
    }

    public void deleteNotification(String notificationId){
        Optional<Notification> existingNotification =
                notificationList.stream().filter(nt -> nt.getNotificationId().equals(notificationId)).findAny();
        if(existingNotification.isEmpty()){
            logger.warning("NO existing notification found with notification ID : " + notificationId);
        }else{
            Notification notification = existingNotification.get();
            notificationList.remove(notification);
            notification.setNotificationStatus(NotificationStatus.DELETED);
            notificationList.add(notification);
        }
    }

}
