package org.example.service;

import org.example.model.Notification;

import java.util.List;
import java.util.logging.Logger;

public class EmailService {

    Logger logger = Logger.getLogger(EmailService.class.getName());

    public void sendEmail(List<String> emails, Notification notification){
        for(String email : emails){
            //sends email for a particular Notification
            logger.info("Email has been sent to " + email + " for Notification Id : " + notification.getNotificationId());
        }
    }
}
