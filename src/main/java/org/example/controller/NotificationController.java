package org.example.controller;

import org.example.service.EmailService;
import org.example.dao.NotificationDao;
import org.example.model.Notification;
import org.example.model.NotificationStatus;

import java.util.List;
import java.util.logging.Logger;

public class NotificationController {

    Logger logger = Logger.getLogger(NotificationController.class.getName());

    private final NotificationDao notificationDao;

    private final EmailService emailService;

    public NotificationController(){
        this.notificationDao = new NotificationDao();
        this.emailService = new EmailService();
    }

    public void createNotification(Notification notification){
        logger.info("Creating New Notification");
        notificationDao.addNotification(notification);
    }

    public void sendEmail(List<String> emails, Notification notification){
        if(notification == null || notification.getNotificationId() == null){
            logger.severe("notification is not correct");
            return;
        }

        emailService.sendEmail(emails, notification);
    }

    public List<Notification> getNotifications(NotificationStatus notificationStatus){
        logger.info("Fetching all notifications with status : " + notificationStatus);
        return notificationDao.getNotifications(notificationStatus);
    }

    public Notification updateNotification(Notification notification){
        logger.info("Updating notification with id : " + notification.getNotificationId());
        return notificationDao.updateNotification(notification);
    }

    public void deleteNotification(String notificationId){
        logger.info("Deleting notification with id : " + notificationId);
        notificationDao.deleteNotification(notificationId);
    }

}
